# Curso JavaScript Coderhouse

## Clase 06 - Arrays

### Incorporar arrays

---

**Tipo de desafío:** 
Complementario.

**Formato:** 
Página HTML y código fuente en JavaScript. Debe identificar el apellido del alumno/a en el nombre de archivo comprimido por "claseApellido".

**Sugerencia:**
Los Arrays cumplen el papel de listas en el programa. Principalmente, los usamos para agrupar elementos de un mismo tipo.
Siempre que sea posible, emplear los métodos disponibles para trabajar con ellos.

---

**>> Consigna:**
Traslada al proyecto integrador el concepto de objetos, visto en la clase de hoy. A partir de los ejemplos mostrados la primera clase, y en función del tipo de simulador que hayas elegido, deberás:

- Incorporar al menos un Array en tu proyecto.

- Utilizar algunos de los métodos o propiedades vistos en la clase.

**>> Aspectos a incluir en el entregable:**
Archivo HTML y Archivo JS, referenciado en el HTML por etiqueta `<script src="js/miarchivo.js"></script>`, que incluya la definición de un algoritmo en JavaScript que emplee array para agrupar elementos similares.

---

**>> Ejemplo:**
Podemos crear arrays para los objetos identificados en el simulador de la clase anterior:

- array de productos
- array de personas
- array de libros
- array de autos
- array de comidas
- array de bebidas
- array de tareas
- etc.

---

### Descripción de la tarea desarrollada

El programa consiste en procesar un pedido de empanadas de un usuario, solicitándole que ingrese la cantidad y sabores.

También posee descuentos por llevar distintas cantidades y descuentos adicionales si se paga con algunas de las opciones de tarjeta de crédito disponibles.
